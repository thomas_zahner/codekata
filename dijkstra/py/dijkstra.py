#!/usr/bin/python

#for details about the adjacency matrix, gpg -d the base64 cyphertext below using passphrase "algo
#jA0EAwMCbktAJloVGm1gyXIEKjUA/FTxS4MKrtp2muugAeF1LI9p+g3kMoSYQiNA4lbkiiLI2fNb8CizgrbAkEZuMMaWcMPXXsbJufrJw/oiwJoTQYMz4XdWvW4+pMFWj4qnr5h30ACUjmATpJ0xwlXH4Cy1AtTM143VcVI64mNZRY4=

adjacency_matrix = [[0, 4, 0, 0, 0, 0, 0, 8, 0],
                   [4, 0, 8, 0, 0, 0, 0, 11, 0],
                   [0, 8, 0, 7, 0, 4, 0, 0, 2],
                   [0, 0, 7, 0, 9, 14, 0, 0, 0],
                   [0, 0, 0, 9, 0, 10, 0, 0, 0],
                   [0, 0, 4, 14, 10, 0, 2, 0, 0],
                   [0, 0, 0, 0, 0, 2, 0, 1, 6],
                   [8, 11, 0, 0, 0, 0, 1, 0, 7],
                   [0, 0, 2, 0, 0, 0, 6, 7, 0]
                  ];

vertex_count = 9

distance_list = [False for x in range(vertex_count)]

processed_list = [False for x in range(vertex_count)]

distance_list[0] = adjacency_matrix[0][0]
#processed_list[0] = True #don't process it, because distance is set on neighoring nodes BEFORE they are processed... when an ancestor node is processed... in inital case the ancestor node of the source/starting node is God (happens outside the main algo loop) :)

def getClosestVertex(adjacency_matrix, distance_list, processed_list, vertex_count):
    closest_vertex = False
    current_minimum_distance = False
    for v in range(vertex_count):
#        print 'V' + str(v)
#        print 'CMD:' + str(current_minimum_distance)
#        print '---'
        if (distance_list[v] is not False and (current_minimum_distance == False or distance_list[v] < current_minimum_distance) and processed_list[v] == False):
            current_minimum_distance = distance_list[v]
            closest_vertex = v
    print 'Returning: ' + str(closest_vertex)
    return closest_vertex

for v in range(vertex_count): #could be a while loop until length of porcessed_list is equal to vertex count?
    closest_vertex = getClosestVertex(adjacency_matrix, distance_list, processed_list, vertex_count)
    print "Found Closest Vertex: " + str(closest_vertex)
    processed_list[closest_vertex] = True

    print "Processing neighbors of: " + str(closest_vertex)
    for v2 in range(vertex_count):
        if (adjacency_matrix[closest_vertex][v2] > 0 and
            processed_list[v2] == False
            and (distance_list[v2] == False or distance_list[v2] > distance_list[closest_vertex] + adjacency_matrix[closest_vertex][v2])):
                print "Processing neighbor: " + str(v2)
                print "Distance to this vertex is: " + str(adjacency_matrix[closest_vertex][v2])
                distance_list[v2] = distance_list[closest_vertex] + adjacency_matrix[closest_vertex][v2]


for vertex in range(vertex_count):
    print str(vertex) + "|" + str(distance_list[vertex])
