#!/usr/bin/nodejs

function Node(value) {
	this.Value = null;
	this.NodeA = null;
	this.NodeB = null;

	this.Value = value;
}

function get_random(max) {
	return Math.floor((Math.random() * max) + 1);
}

function generate_random_tree() {
	var MAX_RANDOM_DEPTH = 10;
	var root = new Node("R");

	function grow_node(node, depth) {
		if(depth > MAX_RANDOM_DEPTH)
			return;

		if(get_random(3) != 3) {
			node.NodeA = new Node(node.Value + "A");
			if(get_random(3) != 3) {
				grow_node(node.NodeA, depth + 1);
			}
		}
		if(get_random(3) != 3) {
			node.NodeB = new Node(node.Value + "B");
			if(get_random(3) != 3) {
				grow_node(node.NodeB, depth + 1);
			}
		}

		return;
	}

	grow_node(root, 0);

	return root;
}

function visit_nodes(node) {
	var q = [];
	q.push(node);

	var current_node = null;

	while(current_node = q.shift()) {
		console.log("Visited: " + current_node.Value);
		if(current_node.NodeA != null)
			q.push(current_node.NodeA);
		if(current_node.NodeB != null)
			q.push(current_node.NodeB);
	}
}

function search_tree(node, search_value) {
	var q = [];
	q.push(node);

	var current_node = null;

	while(current_node = q.shift()) {
		console.log("Searching Node: " + current_node.Value);
		if(current_node.Value == search_value) {
			console.log("SEARCH MATCH!");
			return current_node;
		}
		if(current_node.NodeA != null)
			q.push(current_node.NodeA);
		if(current_node.NodeB != null)
			q.push(current_node.NodeB);
	}
}

function main() {
	process.stdout.write("Generating tree...");
	var root = generate_random_tree();
	console.log("Done!");

	console.log("--- Visiting Nodes ---");
	visit_nodes(root);
	console.log("---");

	var search_value = "RAB";

	console.log("--- Search for '"+search_value+"' ---");
	var found_node = search_tree(root, search_value);
	console.log("---");

	if(found_node != null)
		console.log("Search match at node: "+found_node.Value);
	else
		console.log("Value Not Found!");
}


main();
