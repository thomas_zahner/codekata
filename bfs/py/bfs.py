#!/usr/bin/python

import random

class Node(object):
    def __init__(self, value = None):
        self.a = None
        self.b = None
        self.val = value


def generate_random_tree():
    
    return grow_tree(Node("R"), 0, 5)


def grow_tree(node, depth, maxdepth):
    depth = depth + 1
    if(depth > maxdepth):
        return node
    node.a = Node(node.val + "A")
    if not (random.randint(1,3) == 1):
        grow_tree(node.a, depth, maxdepth)

    node.b = Node(node.val + "B")
    if not (random.randint(1,3) == 1):
        grow_tree(node.b, depth, maxdepth)

    return node


def visit_bfs(node):
    q = []
    q.append(node)

    while (len(q) > 0):
        current_node = q.pop(0)
        print("Visiting: %s" % (current_node.val))
        if not current_node.a is None:
            q.append(current_node.a)
        if not current_node.b is None:
            q.append(current_node.b)


def search_bfs(node, search_value):
    q = []
    q.append(node)

    while (len(q) > 0):
        current_node = q.pop(0)
        print("Searching %s" % (current_node.val))
        if(current_node.val == search_value):
            print("FOUND!")
            return current_node
        if not current_node.a is None:
            q.append(current_node.a)
        if not current_node.b is None:
            q.append(current_node.b)


def main():
    search_value = "RAB"
    print("Generating Random Node Tree")
    root = generate_random_tree()
    print("Done generating tree")

    print("Visiting Tree")
    visit_bfs(root);
    print("Visited Tree!")

    print("Searching for '%s'..." % (search_value))
    found_node = search_bfs(root, search_value)
    if not found_node is None:
        print("Found %s in node %s" % (search_value, found_node.val))
    else:
        print("Did not find value %s" % (search_value))


main()
