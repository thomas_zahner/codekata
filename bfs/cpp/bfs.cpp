#include <queue>
#include <string>
#include <iostream>
#include <cstdlib>
#include <time.h>

class Node
{
	public:
	Node* ChildA = NULL;
	Node* ChildB = NULL;
	std::string Value;

	Node() {}

	Node(std::string initialValue)
	{
		this->Value = initialValue;
	}
};


class TreeHelper
{
	void visitNodesBfs(Node* node)
	{
		std::queue<Node*>* q = new std::queue<Node*>();
		q->push(node);

		while(Node* currentNode = q->front())
		{
			std::cout << "Visiting Node: " << currentNode->Value << std::endl;
			q->push(currentNode->ChildA);
			q->push(currentNode->ChildB);

			q->pop();
		}
	}

	void visitNodesDfsStack(Node* node)
	{
		std::cout << "501: Not Implemented" << std::endl;
		throw 501;
	}

	void visitNodesDfsRecursive(Node* node)
	{
		std::cout << "501: Not Implemented" << std::endl;
		throw 501;
	}

	public:
	enum TraversalType { Bfs = 0, DfsStack, DfsRecursive};

	void VisitNodes(Node* root)
	{
		this->VisitNodes(root, Bfs);
	}
	void VisitNodes(Node* root, TraversalType traversalType)
	{
		if(traversalType == Bfs)
			this->visitNodesBfs(root);
		else if(traversalType == DfsStack)
			this->visitNodesDfsStack(root);
		else
			this->visitNodesDfsRecursive(root);
	}

	Node* SearchTree(Node* root, std::string searchValue, TraversalType traversalType)
	{
		//TODO: refactor to implement multiple traversal strategies
		std::queue<Node*>* q = new std::queue<Node*>();
		q->push(root);

		while(Node* currentNode = q->front())
		{
			std::cout << "Searching Node: " << currentNode->Value << std::endl;
			if(currentNode->Value == searchValue)
			{
				std::cout << "Found '" << searchValue << "' in node '" << currentNode->Value << "'." << std::endl;
				return currentNode;
			}
			q->push(currentNode->ChildA);
			q->push(currentNode->ChildB);

			q->pop();
		}

		return NULL;
	}
};

class TreeGenerator {
	int maxDepth;
	Node* spawnNode(std::string value,  int depth)
	{
		if(depth > this->maxDepth)
			return NULL;

		Node* node = new Node(value);
		if(rand() % 5 != 1)
		{
			node->ChildA = this->spawnNode(value + "A", depth + 1);
		}
		if(rand() % 5 != 1)
		{
			node->ChildB = this->spawnNode(value + "B", depth + 1);
		}

		return node;
	}

	public:
	Node* GenerateRandomTree()
	{
		srand(time(0));
		return this->spawnNode("R", 0);
	}

	TreeGenerator()
	{
		this->maxDepth = 10;
	};
	TreeGenerator(int maxDepth)
	{
		this->maxDepth = maxDepth;
	}
};

int main(void)
{
	Node* root = new Node();

	std::cout << "Generating tree...";
	TreeGenerator* tg = new TreeGenerator(5);
	root = tg->GenerateRandomTree();
	delete tg;
	std::cout << "Done!" << std::endl;

	TreeHelper* th = new TreeHelper();

	std::cout << "Visiting Nodes..." << std::endl;
	th->VisitNodes(root, TreeHelper::Bfs);
//	th->VisitNodes(root, TreeHelper::DfsStack);
	std::cout << "Visted Nodes ---" << std::endl;
	th->SearchTree(root, "RBA", TreeHelper::Bfs);

	return 0;
}
