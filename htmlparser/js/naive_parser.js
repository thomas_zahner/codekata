#!/usr/bin/nodejs

// very naive parser to explore the complexities of lexing and parsing html
// initially pursing an approach to lex and tokenize in a single pass... i assume i will be disabused of this soon.

function Node(tagName, parentNode) {
	this.Children = [];
	this.Name = tagName; //Name == "" => plain text content node
	this.Attributes = [];
	this.Value = "";
	this.Parent = parentNode;
}

function Attribute(attributeName, attributeValue) {
	this.Name = attributeName;
	this.Value = attributeValue;
} 



var html_content = "";

process.stdin.setEncoding('utf8');

process.stdin.on('readable', function() {
	html_content += process.stdin.read();
});

process.stdin.on('end', function () {
	parse_html(html_content);
});


function parse_html(html_content) {	
//	process.stdout.write(html_content);
	var pos = 0;
	var ct = "";
	var in_string = false;
	var cc = "";
	var root = new Node("__root", null);

	var cn = root;
	while(pos < html_content.length) {
		cc = html_content[pos];
// TODO: quotes in text nodes do not escape into string literal mode
// TODO: single quotes (maybe mechanism to includue list of string literal demarcation mechanism), better literal handling
//TODO: SCRIPT tag contents handling </script> text inside of JS string literals...
//TODO: attributue processing
//TODO: better tokenization of HTML elements
//TODO: parsing of opening and closing tags since we are constructing a node tree directly instead of first lexing... can this be done? using a stack during the inital/only lexing/parsing run?

//		if(cc == "<") {
			// start new tag, or error if already inside of tag
			
//		}
		if(in_string && cc != '"') {
			ct += cc;
			pos+=1;
			continue;
		}
		if(in_string && cc == '"') {
			ct += cc;
			pos+=1;
			in_string = false;
			continue;
		}
		if(!in_string && cc == '"') {
			ct+=cc;
			pos+=1;
			in_string = true;
			continue;
		}
		if(!in_string && cc == ">") {
			//terminating tag
			ct += cc;
			pos+=1;
			var nn = new Node(ct, cn);
			cn.Children.push(nn);
			cn = nn;
			ct = "";
			continue;
			// This doesn't actually build the node tree--really it's just creates a Q using a graph node class
			// TODO: make tag recognition go back and see if it's supposed to close a tag... maybe: if is_closing_tag, then recursively crawl up cn.Parent until arriving at corresponding nearest open tag of same type. any children that are closed outside of this will then result in orphaned/abandoned closing tags, or those closing tags will insert an open and close (single node in nthe tree) at the found location of the abandoned closing tag.
		}

		ct += cc;
		pos += 1;
	}
	//console.log(root);
	visit_tree(root);
}

function visit_tree(root) {
	var q = []
	q.push(root);

	var cn = null;
	while(cn = q.shift()) {
		console.log("###");
		console.log(cn.Name);
		console.log("###");
		for(var i=0; i<cn.Children.length; i++) {
			q.push(cn.Children[i]);
		}
	}
}
