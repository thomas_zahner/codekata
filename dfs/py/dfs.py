#!/usr/bin/python

import random

class Node(object):
    def __init__(self, value = None):
        self.a = None
        self.b = None
        self.val = value


def generate_random_tree():
    
    return grow_tree(Node("R"), 0, 5)


def grow_tree(node, depth, maxdepth):
    depth = depth + 1
    if(depth > maxdepth):
        return node
    node.a = Node(node.val + "A")
    if not (random.randint(1,3) == 1):
        grow_tree(node.a, depth, maxdepth)

    node.b = Node(node.val + "B")
    if not (random.randint(1,3) == 1):
        grow_tree(node.b, depth, maxdepth)

    return node


def visit_dfs(node):
    print("Visiting: %s" % (node.val))
    if not node.a is None:
        visit_dfs(node.a)
    if not node.b is None:
        visit_dfs(node.b)


def search_dfs(node, search_value):
    print("Searching %s" % (node.val))
    if(node.val == search_value):
        print("FOUND!")
        return node
    if not node.a is None:
        found_node = search_dfs(node.a, search_value)
        if not found_node is None:
            return found_node
    if not node.b is None:
        found_node = search_dfs(node.b, search_value)
        if not found_node is None:
            return found_node
    return None


def main():
    search_value = "RAB"
    print("Generating Random Node Tree")
    root = generate_random_tree()
    print("Done generating tree")

    print("Visiting Tree")
    visit_dfs(root);
    print("Visited Tree!")

    print("Searching for '%s'..." % (search_value))
    found_node = search_dfs(root, search_value)
    if not found_node is None:
        print("Found %s in node %s" % (search_value, found_node.val))
    else:
        print("Did not find value %s" % (search_value))


main()
