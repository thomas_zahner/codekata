#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>

struct node {
	struct node * nodeA; //must be a pointer; recursion is funny
	struct node * nodeB; //must be a pointer; recursion is funny
	char * value;
};

struct node * new_node(char * value);
struct node * generate_tree();
struct node * search_tree(struct node * node, char * search_value);
void grow_random_tree(struct node * node, int depth);
void visit_node(struct node * node);

int main(void)
{
	srand(time(0));

	printf("Generating tree...");
	struct node * root = new_node("R");
	grow_random_tree(root, 0);
/*
	struct node * root = generate_tree();
*/
	printf("Done!\n");


	printf("Showing generated tree:\n");
	visit_node(root);
	printf("---\n");


	char * search_value = "RBA";

	printf("Searching for '%s'.\n", search_value);
	printf("Beginning tree traversal.\n");
	struct node * found_node = search_tree(root, search_value);
	printf("Done traversing tree.\n");

	if(found_node == NULL)
		printf("Value not found!\n");
	else
		printf("Found value at node '%s'.\n", found_node->value);

	return 0;
}

void visit_node(struct node * node)
{
	printf("Visiting Node: %s\n", node->value);
	if(node->nodeA != NULL)
		visit_node(node->nodeA);
	if(node->nodeB != NULL)
		visit_node(node->nodeB);
}


struct node * search_tree(struct node * node, char * search_value)
{
	printf("Visting: %s\n", node->value);
	if(strcmp(node->value, search_value) == 0)
	{
		printf("Search match at node '%s'\n", node->value);
		return node;
	}
	struct node * found_node = NULL;
	if(node->nodeA != NULL)
	{
		found_node = search_tree(node->nodeA, search_value);
		if(found_node != NULL)
			return found_node;
	}
	if(node->nodeB != NULL)
	{
		found_node = search_tree(node->nodeB, search_value);
		if(found_node != NULL)
			return found_node;
	}
	return NULL;
}

/* Walk before you crawl... fixed test tree structure */
struct node * generate_tree()
{
	struct node * root = new_node("R");
	root->nodeA = new_node("RA");
	root->nodeB = new_node("RB");
	root->nodeA->nodeA = new_node("RAA");
	root->nodeA->nodeB = new_node("RAB");
	root->nodeA->nodeB->nodeB = new_node("RABB");
	root->nodeB->nodeA = new_node("RBA");
	root->nodeB->nodeB = new_node("RBB");
	root->nodeB->nodeA->nodeA = new_node("rBAA");
	return root;
}

/* Let's generate a random tree structure, because #yolo */
int MAX_RANDOM_DEPTH = 3;
char * safe_strcat(char * left, char * right)
{
	char * target = malloc(strlen(left) + strlen(right) + 1);
	strcpy(target, left);
	strcat(target, right);
	return target;
}
void grow_random_tree(struct node * current_node, int current_depth)
{
	if(current_depth>MAX_RANDOM_DEPTH)
		return;
	if(current_node == NULL)
		return;

	//50/50 randomly decide to spawn node
	if(rand()%3 != 0)
	{
		current_node->nodeA = new_node(safe_strcat(current_node->value, "A"));
		if(rand()%3 != 0)
		{
			grow_random_tree(current_node->nodeA, current_depth + 1);
		}
	}

	if(rand()%3 != 0)
	{
		current_node->nodeB = new_node(safe_strcat(current_node->value, "B"));
		if(rand()%3 != 0)
		{
			grow_random_tree(current_node->nodeB, current_depth + 1);
		}
	}
}

struct node * new_node(char * value)
{
	struct node * new_node = malloc(sizeof (struct node));
	new_node->value = value;
	new_node->nodeA = NULL;
	new_node->nodeB = NULL;

	return new_node;
}
