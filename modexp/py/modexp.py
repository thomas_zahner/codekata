#!/usr/bin/python

from sys import argv

if len(argv) < 3:
    b = 5 #base
    e = 14 #exponent
    m = 13 #modulo
else:
    b = int(argv[1])
    e = int(argv[2])
    m = int(argv[3])

#for x in xrange(e): #eventually i will trust xrange is efficient...


p = 1

x = 0
while x < e:
    x = x + 1
    p = p * b % m

print(p)

